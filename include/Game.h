#ifndef GAME
#define GAME

// standard input/output stream
// used for debuging
#include <iostream>
// opengl wrangler library
#include "GL\glew.h"
#include "GL\wglew.h"
// SFML Window and OpenGL libraries
#include "SFML\Window.hpp"
#include "SFML\OpenGL.hpp"

// Project classes
#include "Vector3.h"
#include "Matrix3.h"
#include "Quaternion.h"

class Game
{
public:
	Game(sf::ContextSettings settings = sf::ContextSettings());
	~Game();
	void run();
private:
	void processEvents();
	void processKeyEvents(const sf::Keyboard::Key &);
	void update();
	void render();

	// Main game window
	sf::Window m_window;

	// main game window internal clock
	sf::Clock m_clock;
	// time elapsed between frames
	sf::Time m_elapsed;

	static const unsigned int NUM_VERTICES = 8u;

	// Represents a openGL 3D vertex (contains coordinates and colour)
	struct Vertex
	{
		float coordinate[3];
		float color[3];
	} m_vertices[NUM_VERTICES];

	const float HALF_WIDTH = 0.5f;

	static const unsigned int NUM_INDEXES = 36u;

	GLubyte m_indexes[NUM_INDEXES];

	// storing the sizeof(Glubyte)
	static const unsigned int SIZE_OF_INDEXES = sizeof(GLubyte) * NUM_INDEXES;


	/* Variable to hold the VBO identifier */
	GLuint m_vbo = 1u;
	GLuint m_vboIndex = 1u;

	// storing the sizeof(Vertex)
	static const unsigned int SIZE_OF_VERTEX = sizeof(Vertex);

	Matrix3 m_transform;

	Quaternion m_transformQ;
};

#endif // !GAME