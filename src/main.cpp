/// <summary>
/// @mainpage OpenGL Practical 8
/// @Author Rafael Girao
/// @Version 1.0
/// @brief Program will
///
/// Time Spent:
///		08th/12/2016 14:00 60min
///
/// Total Time Taken:
///		1hr 00min
/// </summary>

#include "Game.h"

int main()
{
	sf::ContextSettings settings;
	settings.antialiasingLevel = 32u;
	Game & game = Game(settings);
	game.run();
	return EXIT_SUCCESS;
}