#include "Game.h"


/// <summary>
/// Game constructor
/// used to initialize all game elements
/// </summary>
Game::Game(sf::ContextSettings settings) :
	m_window(sf::VideoMode(800u, 600u, 32u), "OpenGL Vertex Buffer", sf::Style::Default, settings)
{
	glewInit();

	/* Vertices counter-clockwise winding */

	// right-top-front
	m_vertices[0].coordinate[0] = HALF_WIDTH;
	m_vertices[0].coordinate[1] = HALF_WIDTH;
	m_vertices[0].coordinate[2] = HALF_WIDTH;
	m_vertices[0].color[0] = 0.0f;
	m_vertices[0].color[1] = 1.0f;
	m_vertices[0].color[2] = 0.0f;

	// left-top-front
	m_vertices[1].coordinate[0] = -HALF_WIDTH;
	m_vertices[1].coordinate[1] = HALF_WIDTH;
	m_vertices[1].coordinate[2] = HALF_WIDTH;
	m_vertices[1].color[0] = 1.0f;
	m_vertices[1].color[1] = 0.0f;
	m_vertices[1].color[2] = 0.0f;

	// left-bottom-front
	m_vertices[2].coordinate[0] = -HALF_WIDTH;
	m_vertices[2].coordinate[1] = -HALF_WIDTH;
	m_vertices[2].coordinate[2] = HALF_WIDTH;
	m_vertices[2].color[0] = 0.0f;
	m_vertices[2].color[1] = 0.0f;
	m_vertices[2].color[2] = 1.0f;

	// right-bottom-front
	m_vertices[3].coordinate[0] = HALF_WIDTH;
	m_vertices[3].coordinate[1] = -HALF_WIDTH;
	m_vertices[3].coordinate[2] = HALF_WIDTH;
	m_vertices[3].color[0] = 1.0f;
	m_vertices[3].color[1] = 0.0f;
	m_vertices[3].color[2] = 0.0f;

	// right-top-back
	m_vertices[4].coordinate[0] = HALF_WIDTH;
	m_vertices[4].coordinate[1] = HALF_WIDTH;
	m_vertices[4].coordinate[2] = -HALF_WIDTH;
	m_vertices[4].color[0] = 0.0f;
	m_vertices[4].color[1] = 1.0f;
	m_vertices[4].color[2] = 0.0f;

	//left-top-back
	m_vertices[5].coordinate[0] = -HALF_WIDTH;
	m_vertices[5].coordinate[1] = HALF_WIDTH;
	m_vertices[5].coordinate[2] = -HALF_WIDTH;
	m_vertices[5].color[0] = 1.0f;
	m_vertices[5].color[1] = 0.0f;
	m_vertices[5].color[2] = 0.0f;

	// left-bottom-back
	m_vertices[6].coordinate[0] = -HALF_WIDTH;
	m_vertices[6].coordinate[1] = -HALF_WIDTH;
	m_vertices[6].coordinate[2] = -HALF_WIDTH;
	m_vertices[6].color[0] = 0.0f;
	m_vertices[6].color[1] = 0.0f;
	m_vertices[6].color[2] = 1.0f;

	// right-bottom-back
	m_vertices[7].coordinate[0] = HALF_WIDTH;
	m_vertices[7].coordinate[1] = -HALF_WIDTH;
	m_vertices[7].coordinate[2] = -HALF_WIDTH;
	m_vertices[7].color[0] = 1.0f;
	m_vertices[7].color[1] = 0.0f;
	m_vertices[7].color[2] = 0.0f;


	/* Vertex Index initialization */

	// front face
	m_indexes[0] = 0u;   m_indexes[1] = 1u;   m_indexes[2] = 2u;
	m_indexes[3] = 3u;   m_indexes[4] = 0u;   m_indexes[5] = 2u;

	// right face
	m_indexes[6] = 0u; m_indexes[7] = 3u; m_indexes[8] = 4u;
	m_indexes[9] = 4u; m_indexes[10] = 3u; m_indexes[11] = 7u;

	// back face
	m_indexes[12] = 4u;   m_indexes[13] = 6u;   m_indexes[14] = 5u;
	m_indexes[15] = 7u;   m_indexes[16] = 6u;   m_indexes[17] = 4u;

	// left face
	m_indexes[18] = 5u;   m_indexes[19] = 6u;   m_indexes[20] = 1u;
	m_indexes[21] = 1u;   m_indexes[22] = 6u;   m_indexes[23] = 2u;

	// top face
	m_indexes[24] = 5u;   m_indexes[25] = 1u;   m_indexes[26] = 4u;
	m_indexes[27] = 0u;   m_indexes[28] = 4u;   m_indexes[29] = 1u;

	// bottom face
	m_indexes[30] = 7u;   m_indexes[31] = 2u;   m_indexes[32] = 6u;
	m_indexes[33] = 3u;   m_indexes[34] = 2u;   m_indexes[35] = 7u;

	/* Create a new VBO using VBO id */
	glGenBuffers(1, &m_vbo);

	/* Bind the VBO */
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);

	/* Upload vertex data to GPU */
	glBufferData(GL_ARRAY_BUFFER, (SIZE_OF_VERTEX * NUM_VERTICES), m_vertices, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);

	glGenBuffers(1, &m_vboIndex);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_vboIndex);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, SIZE_OF_INDEXES, m_indexes, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_vboIndex);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0, m_window.getSize().x / m_window.getSize().y, 0.0, 6.0);
	glMatrixMode(GL_MODELVIEW);
	glTranslatef(0.0f, 0.0f, -3.0f);

	// activate openGL culling
	glEnable(GL_CULL_FACE);
}

/// <summary>
/// Game destructor
/// </summary>
Game::~Game()
{
}

void Game::run()
{
	while (m_window.isOpen())
	{
		std::cout << "Game running..." << std::endl;
		processEvents();
		update();
		render();
	}
}

void Game::processEvents()
{
	sf::Event event;
	while (m_window.pollEvent(event))
	{
		switch (event.type)
		{
		case sf::Event::Closed:
			m_window.close();
			break;
		case sf::Event::KeyPressed:
			processKeyEvents(event.key.code);
			break;
		default:
			break;
		}
	}
}

void Game::processKeyEvents(const sf::Keyboard::Key & keyPressed)
{
	const float MOVEMENT = 0.02f;
	const double ANGLE = 1.0;
	const double SCALE_UP = 1.1;
	const double SCALE_DOWN = 0.9;
	const double TRANSLATE = 0.2;

	const Quaternion TRANSFORM_Q = Quaternion(0.0, 0.0, 1.0, 0.0);

	Vector3 point = Vector3(0.0, 0.0, 0.0);

	switch (keyPressed)
	{
	case sf::Keyboard::Escape:
		m_window.close();
		break;
	case sf::Keyboard::A:
		for (auto & vertex : m_vertices)
		{
			vertex.coordinate[0] -= MOVEMENT;
		}
		break;
	case sf::Keyboard::D:
		for (auto & vertex : m_vertices)
		{
			vertex.coordinate[0] += MOVEMENT;
		}
		break;
	case sf::Keyboard::W:
		for (auto & vertex : m_vertices)
		{
			vertex.coordinate[1] += MOVEMENT;
		}
		break;
	case sf::Keyboard::S:
		for (auto & vertex : m_vertices)
		{
			vertex.coordinate[1] -= MOVEMENT;
		}
		break;
	case sf::Keyboard::Up:
		m_transform = Matrix3::rotation(Matrix3::Axis::X, ANGLE);
		for (auto & vertex : m_vertices)
		{
			point = Vector3(vertex.coordinate[0], vertex.coordinate[1], vertex.coordinate[2]);
			point = m_transform * point;
			vertex.coordinate[0] = static_cast<float>(point.getX());
			vertex.coordinate[1] = static_cast<float>(point.getY());
			vertex.coordinate[2] = static_cast<float>(point.getZ());
		}
		break;
	case sf::Keyboard::Down:
		m_transform = Matrix3::rotation(Matrix3::Axis::X, -ANGLE);
		for (auto & vertex : m_vertices)
		{
			point = Vector3(vertex.coordinate[0], vertex.coordinate[1], vertex.coordinate[2]);
			point = m_transform * point;
			vertex.coordinate[0] = static_cast<float>(point.getX());
			vertex.coordinate[1] = static_cast<float>(point.getY());
			vertex.coordinate[2] = static_cast<float>(point.getZ());
		}
		break;
	case sf::Keyboard::Left:
		for (auto & vertex : m_vertices)
		{
			point = Vector3(vertex.coordinate[0], vertex.coordinate[1], vertex.coordinate[2]);
			point = TRANSFORM_Q.rotate(point, -ANGLE);
			vertex.coordinate[0] = static_cast<float>(point.getX());
			vertex.coordinate[1] = static_cast<float>(point.getY());
			vertex.coordinate[2] = static_cast<float>(point.getZ());
		}
		break;
	case sf::Keyboard::Right:
		for (auto & vertex : m_vertices)
		{
			point = Vector3(vertex.coordinate[0], vertex.coordinate[1], vertex.coordinate[2]);
			point = TRANSFORM_Q.rotate(point, ANGLE);
			vertex.coordinate[0] = static_cast<float>(point.getX());
			vertex.coordinate[1] = static_cast<float>(point.getY());
			vertex.coordinate[2] = static_cast<float>(point.getZ());
		}
		break;
	case sf::Keyboard::X:
		m_transform = Matrix3::scale(SCALE_DOWN, SCALE_DOWN, SCALE_DOWN);
		for (auto & vertex : m_vertices)
		{
			point = Vector3(vertex.coordinate[0], vertex.coordinate[1], vertex.coordinate[2]);
			point = m_transform * point;
			vertex.coordinate[0] = static_cast<float>(point.getX());
			vertex.coordinate[1] = static_cast<float>(point.getY());
			vertex.coordinate[2] = static_cast<float>(point.getZ());
		}
		break;
	case sf::Keyboard::Z:
		m_transform = Matrix3::scale(SCALE_UP, SCALE_UP, SCALE_UP);
		for (auto & vertex : m_vertices)
		{
			point = Vector3(vertex.coordinate[0], vertex.coordinate[1], vertex.coordinate[2]);
			point = m_transform * point;
			vertex.coordinate[0] = static_cast<float>(point.getX());
			vertex.coordinate[1] = static_cast<float>(point.getY());
			vertex.coordinate[2] = static_cast<float>(point.getZ());
		}
		break;
	case sf::Keyboard::Q:
		m_transform = Matrix3::translate(-TRANSLATE, 0.0);
		for (auto & vertex : m_vertices)
		{
			point = Vector3(vertex.coordinate[0], vertex.coordinate[1], vertex.coordinate[2]);
			point = m_transform * point;
			vertex.coordinate[0] = static_cast<float>(point.getX());
			vertex.coordinate[1] = static_cast<float>(point.getY());
			vertex.coordinate[2] = static_cast<float>(point.getZ());
		}
		break;
	case sf::Keyboard::E:
		m_transform = Matrix3::translate(TRANSLATE, 0.0);
		for (auto & vertex : m_vertices)
		{
			point = Vector3(vertex.coordinate[0], vertex.coordinate[1], vertex.coordinate[2]);
			point = m_transform * point;
			vertex.coordinate[0] = static_cast<float>(point.getX());
			vertex.coordinate[1] = static_cast<float>(point.getY());
			vertex.coordinate[2] = static_cast<float>(point.getZ());
		}
		break;
	default:
		break;
	}
}

void Game::update()
{
	std::cout << "Update" << std::endl;

	m_elapsed = m_clock.getElapsedTime();

}

void Game::render()
{
	std::cout << "Drawing" << std::endl;

	glCullFace(GL_BACK);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	// upload vertices to GPU
	glBufferData(GL_ARRAY_BUFFER, (SIZE_OF_VERTEX * NUM_VERTICES), m_vertices, GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, m_vbo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_vboIndex);

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_COLOR_ARRAY);

	glColorPointer(3, GL_FLOAT, SIZE_OF_VERTEX, reinterpret_cast<void *>(NULL + (SIZE_OF_VERTEX / 2u)));

	/* Draw Triangle from VBO */

	glVertexPointer(3, GL_FLOAT, SIZE_OF_VERTEX, reinterpret_cast<void *>(NULL + 0));
	glDrawElements(GL_TRIANGLES, NUM_INDEXES, GL_UNSIGNED_BYTE, reinterpret_cast<void *>(NULL + 0));

	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_COLOR_ARRAY);

	m_window.display();
}
